# String Splitter

A mod that controls which capes are displayed in the game.

## Config

The default config is as follows:

```json
{
    "allowCapesByDefault": true,
    "capeExceptions": []
}
```

Options:

| Key | Default | Description |
|-----|---------|-------------|
| `allowCapesByDefault` | `true` | If true, then all capes are shown unless they appear in `capeExceptions`. If false, then all capes are hidden by default and only the ones in `capeExceptions` are shown. |
| `capeExceptions` | `[]` | List of cape URLs. Unfortunately, there isn’t a good way to get them other than by using the API to get the profile info of a player with the cape in question. |

### Some cape URLs

* Migrator: https://textures.minecraft.net/texture/2340c0e03dd24a11b15a8b33c2a7e9e32abb2051b2481d0ba7defd635ca7a933
* Vanilla: https://textures.minecraft.net/texture/f9a76537647989f9a0b6d001e320dac591c359e9e61a31f4ce11c88f207f0ad4
* Cherry: https://textures.minecraft.net/texture/afd553b39358a24edfe3b8a9a939fa5fa4faa4d9a9c3d6af8eafb377fa05c2bb
