package xyz.flirora.stringsplitter.mixin;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import net.minecraft.client.texture.PlayerSkinProvider;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;
import xyz.flirora.stringsplitter.StringSplitter;

import java.util.Map;

@Mixin(PlayerSkinProvider.class)
public class PlayerListEntryMixin {
    @Unique private static void stringsplitter$modifyProfileTexture(Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> map, MinecraftProfileTexture.Type type) {
        map.compute(type, (key, val) -> {
            if (val == null) return val;
            if (StringSplitter.CONFIG.shouldHideCape(val.getUrl())) {
                return null;
            }
            return val;
        });
    }

    @Inject(at = @At(value = "INVOKE", target = "Ljava/util/Map;putAll(Ljava/util/Map;)V", shift = At.Shift.AFTER), method = "method_4653", locals = LocalCapture.CAPTURE_FAILSOFT)
    private void onLoadSkinTexture(GameProfile gameProfile, boolean bl, PlayerSkinProvider.SkinTextureAvailableCallback skinTextureAvailableCallback, CallbackInfo ci, Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> map) {
        stringsplitter$modifyProfileTexture(map, MinecraftProfileTexture.Type.CAPE);
        stringsplitter$modifyProfileTexture(map, MinecraftProfileTexture.Type.ELYTRA);
    }
}