package xyz.flirora.stringsplitter;

import com.google.common.collect.ImmutableSet;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;

public class StringSplitterConfig {
    public static final String CONFIG_PATH_STR = "config/stringsplitter.json";
    public static final Path CONFIG_PATH = Path.of(CONFIG_PATH_STR);
    public static final Path CONFIG_DIR = CONFIG_PATH.getParent();
    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().serializeNulls().setLenient().create();
    public boolean allowCapesByDefault = true;
    public Set<String> capeExceptions = ImmutableSet.of();

    public StringSplitterConfig() {
    }

    public static StringSplitterConfig readFromFile() {
        StringSplitterConfig config;
        try (FileReader fh = new FileReader(CONFIG_PATH_STR, StandardCharsets.UTF_8)) {
            try {
                config = GSON.fromJson(fh, StringSplitterConfig.class);
                // Perform validation (none currently)
            } catch (JsonIOException | JsonSyntaxException e) {
                StringSplitter.LOGGER.error("Invalid config: ", e);
                StringSplitter.LOGGER.error("Falling back to default config");
                return new StringSplitterConfig();
            }
        } catch (IOException e) {
            // Create new config
            config = new StringSplitterConfig();
            if (!Files.exists(CONFIG_PATH)) {
                try {
                    Files.createDirectories(CONFIG_DIR);
                } catch (IOException e2) {
                    StringSplitter.LOGGER.error("Could not create config directory: ", e);
                }
            } else {
                StringSplitter.LOGGER.error("Could not read from config file:  ", e);
            }
        }
        try (FileWriter fh = new FileWriter(CONFIG_PATH_STR, StandardCharsets.UTF_8)) {
            GSON.toJson(config, fh);
        } catch (IOException ex) {
            StringSplitter.LOGGER.error("Could not write to config file: ", ex);
        }
        return config;
    }

    public boolean shouldHideCape(String url) {
        return this.capeExceptions.contains(url) == this.allowCapesByDefault;
    }
}
