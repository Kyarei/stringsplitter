package xyz.flirora.stringsplitter;


import com.mojang.logging.LogUtils;
import org.slf4j.Logger;

public class StringSplitter {
    public static final String MOD_ID = "stringsplitter";
    public static final Logger LOGGER = LogUtils.getLogger();
    public static final StringSplitterConfig CONFIG = StringSplitterConfig.readFromFile();
    
    public static void init() {
        LOGGER.info("Initializing String Splitter");
    }
}
