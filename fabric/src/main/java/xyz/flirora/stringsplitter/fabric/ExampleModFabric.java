package xyz.flirora.stringsplitter.fabric;

import xyz.flirora.stringsplitter.StringSplitter;
import net.fabricmc.api.ModInitializer;

public class ExampleModFabric implements ModInitializer {
    @Override
    public void onInitialize() {
        StringSplitter.init();
    }
}
