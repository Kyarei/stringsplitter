package xyz.flirora.stringsplitter.forge;

import xyz.flirora.stringsplitter.StringSplitter;
import net.minecraftforge.fml.common.Mod;

@Mod(StringSplitter.MOD_ID)
public class ExampleModForge {
    public ExampleModForge() {
        // Submit our event bus to let architectury register our content on the right time
        StringSplitter.init();
    }
}
